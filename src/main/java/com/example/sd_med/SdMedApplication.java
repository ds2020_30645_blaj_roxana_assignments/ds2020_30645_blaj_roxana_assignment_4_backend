package com.example.sd_med;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SdMedApplication {

    public static void main(String[] args) {
        SpringApplication.run(SdMedApplication.class, args);
    }

}
