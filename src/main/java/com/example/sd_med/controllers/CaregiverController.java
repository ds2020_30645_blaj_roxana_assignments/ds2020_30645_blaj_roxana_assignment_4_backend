package com.example.sd_med.controllers;

import com.example.sd_med.dtos.CaregiverDTO;
import com.example.sd_med.services.CaregiverService;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping(value = "/caregiver")
public class CaregiverController {

    private final CaregiverService caregiverService;

    @Autowired
    public CaregiverController(CaregiverService caregiverService) {
        this.caregiverService = caregiverService;
    }

    @GetMapping()
    public ResponseEntity<List<CaregiverDTO>> getCaregivers() {
        List<CaregiverDTO> dtos = caregiverService.findCaregivers();
        for (CaregiverDTO dto : dtos) {
            Link patientLink = linkTo(methodOn(PatientController.class)
                    .getPatientById(dto.getId())).withRel("caregiverDetails");
            dto.add(patientLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(value = "/id/{id}")
    public ResponseEntity<CaregiverDTO> getCaregiverById(@PathVariable("id") UUID patientID) {
        CaregiverDTO dto = caregiverService.findCaregiverById(patientID);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @GetMapping(value = "/name/{name}")
    public ResponseEntity<CaregiverDTO> getCaregiverByName(@PathVariable("name") String name) {
        CaregiverDTO dto = caregiverService.findCaregiverByName(name);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @PutMapping(value = "/{name}")
    public ResponseEntity<CaregiverDTO> updateCaregiver(@PathVariable("name") String name, @Valid @RequestBody CaregiverDTO caregiverDTO){
        CaregiverDTO dto = caregiverService.updateCaregiver(name, caregiverDTO);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }



}
