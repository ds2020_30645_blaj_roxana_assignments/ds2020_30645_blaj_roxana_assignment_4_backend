package com.example.sd_med.controllers;

import com.example.sd_med.dtos.MedicationDTO;
import com.example.sd_med.services.MedicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping(value="/medication")
public class MedicationController {

    private final MedicationService medicationService;

    @Autowired
    public MedicationController(MedicationService medicationService){this.medicationService=medicationService;}

    @GetMapping(value = "/{id}")
    public ResponseEntity<MedicationDTO> getMedication(@PathVariable("id") UUID medicationID) {
        MedicationDTO dto = medicationService.findMedicationById(medicationID);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @GetMapping()
    public ResponseEntity<List<MedicationDTO>> getMedications() {
        List<MedicationDTO> dtos = medicationService.findMedications();
        for (MedicationDTO dto : dtos) {
            Link medicationLink = linkTo(methodOn(MedicationController.class)
                    .getMedication(dto.getId())).withRel("medicationDetails");
            dto.add(medicationLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<UUID> insert(@Valid @RequestBody MedicationDTO medicationDTO) {
        UUID medicationID = medicationService.insert(medicationDTO);
        return new ResponseEntity<>(medicationID, HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/id/{id}")
    public void deleteMedication(@PathVariable("id") UUID id) {
        medicationService.deleteMedicationById(id);
    }

    @DeleteMapping(value = "/name/{name}")
    public ResponseEntity<UUID> deleteMedication(@PathVariable("name") String name ) {
        UUID medicationID = medicationService.deleteMedicationByName(name);
        return new ResponseEntity<>(medicationID, HttpStatus.OK);
    }

    @PutMapping(value = "/id/{id}")
    public ResponseEntity<MedicationDTO> updateMedication(@PathVariable("id") UUID id, @Valid @RequestBody MedicationDTO medicationDTO){
        MedicationDTO dto = medicationService.updateMedication(id, medicationDTO);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @PutMapping(value = "/name/{name}")
    public ResponseEntity<UUID> updateMedication(@PathVariable("name") String name, @Valid @RequestBody MedicationDTO medicationDTO){
        UUID dto = medicationService.updateMedicationByName(name, medicationDTO);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

}
