package com.example.sd_med.controllers;

import com.example.sd_med.dtos.MedicationPlanDTO;
import com.example.sd_med.entities.MedicationPlan;
import com.example.sd_med.services.MedicationPlanService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/medicationPlan")
public class MedicationPlanController {

    private final MedicationPlanService medicationPlanService;

    public MedicationPlanController(MedicationPlanService medicationPlanService) {
        this.medicationPlanService = medicationPlanService;
    }

    @GetMapping()
    public ResponseEntity<List<MedicationPlanDTO>> getMedPlans() {
        List<MedicationPlanDTO> dtos = medicationPlanService.getMedicationPlans();
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<MedicationPlanDTO> getMedPlanByID(@PathVariable("id") UUID id) {
        MedicationPlanDTO dto = medicationPlanService.getMedPlanByID(id);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @PostMapping(value = "/add")
    public ResponseEntity<MedicationPlan> addMedPlan(@Valid @RequestBody MedicationPlanDTO medicationPlanDTO) {
        MedicationPlan medicationPlan= medicationPlanService.addMedicationPlan(medicationPlanDTO);
        return new ResponseEntity<>(medicationPlan, HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/{id}")
    public  void deleteMedicationPlan(@PathVariable("id") UUID id) {
        medicationPlanService.deleteMedPlanByID(id);
    }



}
