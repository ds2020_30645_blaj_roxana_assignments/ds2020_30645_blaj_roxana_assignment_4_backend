package com.example.sd_med.controllers;

import com.example.sd_med.dtos.MedicalRecordDTO;
import com.example.sd_med.dtos.PatientDTO;
import com.example.sd_med.services.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping(value = "/patient")
public class PatientController {

    private final PatientService patientService;

    @Autowired
    public PatientController(PatientService patientService) {
        this.patientService = patientService;
    }

    @GetMapping()
    public ResponseEntity<List<PatientDTO>> getPatients() {
        List<PatientDTO> dtos = patientService.findPatient();
        for (PatientDTO dto : dtos) {
            Link patientLink = linkTo(methodOn(PatientController.class)
                    .getPatientById(dto.getId())).withRel("patientDetails");
            dto.add(patientLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(value = "/id/{id}")
    public ResponseEntity<PatientDTO> getPatientById(@PathVariable("id") UUID patientID) {
        PatientDTO dto = patientService.findPatientById(patientID);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @GetMapping(value = "/name/{name}")
    public ResponseEntity<PatientDTO> getPatientByName(@PathVariable("name") String name) {
        PatientDTO dto = patientService.findPatientByName(name);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @PutMapping(value = "/{name}")
    public ResponseEntity<PatientDTO> updatePatient(@PathVariable("name") String name, @Valid @RequestBody PatientDTO patientDTO){
        PatientDTO dto = patientService.updatePatient(name, patientDTO);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @PutMapping(value = "/updateMedicalRecord/{name}")
    public ResponseEntity<PatientDTO> updateMedicalRecord(@PathVariable("name") String name, @Valid @RequestBody MedicalRecordDTO medicalRecord){
        PatientDTO dto = patientService.updateMedicalRecord(name, medicalRecord);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @PutMapping(value = "/name/{name}/medPlan/{id}")
    public ResponseEntity<PatientDTO> addMedPlanToPatient(@PathVariable("name") String patientName, @PathVariable("id") UUID id){
        PatientDTO dto = patientService.updateMedPlan(patientName, id);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }
}
