package com.example.sd_med.controllers;

import com.example.sd_med.dtos.*;
import com.example.sd_med.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.persistence.SecondaryTable;
import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping(value="/userr")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService){ this.userService=userService;}


    @PostMapping()
    public ResponseEntity<UUID> createUser(@Valid @RequestBody UserDTO userDTO) {
        UUID userID = userService.createUser(userDTO);
        return new ResponseEntity<>(userID, HttpStatus.CREATED);
    }

    @GetMapping()
    public ResponseEntity<List<UserDTO>> getUsers() {
        List<UserDTO> dtos = userService.findUser();
        for (UserDTO dto : dtos) {
            Link userLink = linkTo(methodOn(UserController.class)
                    .getUser(dto.getId())).withRel("personDetails");

            dto.add(userLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<UserDTO> getUser(@PathVariable("id") UUID userId) {
        UserDTO dto = userService.findUserById(userId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @DeleteMapping(value = "/id/{id}")
    public void deleteUserById(@PathVariable("id") UUID id) {
        userService.deleteUserById(id);
    }

    @DeleteMapping(value = "/username/{username}")
    public ResponseEntity<UUID> deleteUserByName(@PathVariable("username") String username) {
        UUID userID = userService.deleteUserByUsername(username);
        return new ResponseEntity<>(userID, HttpStatus.OK);
    }


    @PostMapping(value="/doctor")
    //@Secured("DOCTOR")
    //@PreAuthorize("hasRole('DOCTOR')")
    public ResponseEntity<UUID> createUserDoctor(@Valid @RequestBody UserDoctorDTO userDoctorDTO) {
        UUID userID = userService.createUserDoctor(userDoctorDTO);
        return new ResponseEntity<>(userID, HttpStatus.CREATED);
    }

    @PostMapping(value="/patient")
    public ResponseEntity<UUID> createUserPatient(@Valid @RequestBody UserPatientDTO userPatientDTO) {
        UUID userID = userService.createUserPatient(userPatientDTO);
        return new ResponseEntity<>(userID, HttpStatus.CREATED);
    }


    @PostMapping(value="/caregiver")
    public ResponseEntity<UUID> createUserCaregiver(@Valid @RequestBody UserCaregiverDTO userCaregiverDTO) {
        UUID userID = userService.createUserCaregiver(userCaregiverDTO);
        return new ResponseEntity<>(userID, HttpStatus.CREATED);
    }

  /*  @PostMapping(value="/login")
    public ResponseEntity<UserDTO> loginUser( @RequestBody LoginInfoDTO loginInfoDTO) {
        UserDTO userR = userService.login(loginInfoDTO);
        return new ResponseEntity<UserDTO>(userR, HttpStatus.OK);
    } */

   @PostMapping(value="/getRoleFor/{username}")
   public ResponseEntity<String> getRoleForUser(@PathVariable("username") String username) {
        if( userService.getRole(username)!=null){
            String role = userService.getRole(username);
           return new ResponseEntity<>(role, HttpStatus.OK);
       }else return new ResponseEntity<>("", HttpStatus.NOT_FOUND );
   }

    @GetMapping(value="/caregiver/{username}")
    public ResponseEntity<String> getCaregiverWIthUsername(@PathVariable("username") String username) {
        if(userService.getCaregiverWithUsername(username)!=null) {
            String name = userService.getCaregiverWithUsername(username);
            return new ResponseEntity<>(name, HttpStatus.OK);
        }else return new ResponseEntity<>("", HttpStatus.NOT_FOUND );
    }

    @GetMapping(value="/patient/{username}")
    public ResponseEntity<String> getPatientWIthUsername(@PathVariable("username") String username) {
        if(userService.getPatientWithUsername(username)!=null){
            String name = userService.getPatientWithUsername(username);
            return new ResponseEntity<>(name, HttpStatus.OK);
        }
        else return new ResponseEntity<>("", HttpStatus.NOT_FOUND );

    }

    @GetMapping(value = "/patientCaregiver")
    public ResponseEntity<List<PatientCaregiverDTO>> getPatientCaregiv() {
        List<PatientCaregiverDTO> dtos = userService.findPatientCaregiver();
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(value = "/caregiverPatients/name/{name}")
    public ResponseEntity<List<PatientDTO>> getPatientsForACaregiver(@PathVariable("name") String name ) {
        List<PatientDTO> dtos = userService.findPatientForC(name);
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }


   /* @PutMapping(value="/addPatientFor/{username}")
    public ResponseEntity<UUID> addPatientForCaregiver(@PathVariable("username") String username, @Valid @RequestBody PatientDTO patientDTO ){
        UUID idC= userService.addPatientToCaregiver(username, patientDTO);
        return new ResponseEntity<>(idC, HttpStatus.CREATED);
    }*/



}
