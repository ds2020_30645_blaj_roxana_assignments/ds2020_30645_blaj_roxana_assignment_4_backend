package com.example.sd_med.dtos;

import org.springframework.hateoas.RepresentationModel;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class CaregiverDTO extends RepresentationModel<MedicationDTO> {

    private UUID id;
    @NotNull
    @Pattern(regexp = "^[a-zA-Z0-9._ ]+$")

    private String name;
    @NotNull
    //@Pattern(regexp = "^(19|20)\\d\\d[-](0[1-9]|1[012])[-](0[1-9]|[12][0-9]|3[01])")
    private Date birthdate;
    @NotNull
    private String gender;
    @NotNull
    private String address;



    public CaregiverDTO() {
    }

    public CaregiverDTO(UUID id, String name, Date birthdate, String gender, String address) {
        this.id = id;
        this.name = name;
        this.birthdate = birthdate;
        this.gender = gender;
        this.address = address;

    }

    public CaregiverDTO(String name,  Date birthdate,  String gender,  String address) {
        this.name = name;
        this.birthdate = birthdate;
        this.gender = gender;
        this.address = address;
    }


    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }


}
