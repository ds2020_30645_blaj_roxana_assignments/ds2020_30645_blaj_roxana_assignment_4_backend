package com.example.sd_med.dtos;

public class CaregiverPatientDTO {

    private String name; //caregiver name
    private PatientDTO patientDTO;

    public CaregiverPatientDTO(String name, PatientDTO patientDTO) {
        this.name = name;
        this.patientDTO = patientDTO;
    }

    public CaregiverPatientDTO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PatientDTO getPatientDTO() {
        return patientDTO;
    }

    public void setPatientDTO(PatientDTO patientDTO) {
        this.patientDTO = patientDTO;
    }
}
