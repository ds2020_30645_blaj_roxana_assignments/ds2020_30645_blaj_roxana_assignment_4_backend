package com.example.sd_med.dtos;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.UUID;

public class DoctorDTO {

    private UUID id;
    @NotNull
    @NotEmpty
    @Pattern(regexp = "^[a-zA-Z0-9._ ]+$")
    private String name;
    @NotNull
    @NotEmpty
    private String specialization;

    public DoctorDTO(UUID id, String name, String specialization) {
        this.id = id;
        this.name=name;
        this.specialization = specialization;
    }

    public DoctorDTO(String name, String specialization) {
        this.name=name;
        this.specialization = specialization;
    }

    public DoctorDTO() {
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
