package com.example.sd_med.dtos;

public class MedicalRecordDTO {

    private String medicalRecord;

    public MedicalRecordDTO(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

    public MedicalRecordDTO() {
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }
}
