package com.example.sd_med.dtos;

import com.example.sd_med.dtos.validators.annotation.DosageLimit;
import org.springframework.hateoas.RepresentationModel;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.UUID;

public class MedicationDTO extends RepresentationModel<MedicationDTO> {

    public UUID id;
    @NotNull
    @NotEmpty
    public String name;
    @NotNull
    @NotEmpty
    public String sideEffects;
    @NotNull
    @DosageLimit
    public double dosage;

    public MedicationDTO() {
    }

    public MedicationDTO(@NotNull String name, @NotNull String sideEffects, @NotNull double dosage) {
        this.name = name;
        this.sideEffects = sideEffects;
        this.dosage = dosage;
    }

    public MedicationDTO(UUID id, @NotNull String name, @NotNull String sideEffects, @NotNull double dosage) {
        this.id = id;
        this.name = name;
        this.sideEffects = sideEffects;
        this.dosage = dosage;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSideEffects() {
        return sideEffects;
    }

    public void setSideEffects(String sideEffects) {
        this.sideEffects = sideEffects;
    }

    public double getDosage() {
        return dosage;
    }

    public void setDosage(double dosage) {
        this.dosage = dosage;
    }

}
