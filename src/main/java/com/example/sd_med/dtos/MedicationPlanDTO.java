package com.example.sd_med.dtos;

import com.example.sd_med.entities.Medication;

import java.util.List;
import java.util.UUID;

public class MedicationPlanDTO {

    private UUID id;
    private String dailyInterval;
    private int TratamentPeriod;
    private List<Medication> medList;

    public MedicationPlanDTO(){}

    public MedicationPlanDTO(UUID id, String dailyInterval, int TratamentPeriod, List<Medication> medList) {
        this.id = id;
        this.dailyInterval = dailyInterval;
        this.TratamentPeriod = TratamentPeriod;
        this.medList = medList;
    }

    public MedicationPlanDTO(String interval, int TratamentPeriod, List<Medication> medList) {
        this.dailyInterval = dailyInterval;
        this.TratamentPeriod = TratamentPeriod;
        this.medList = medList;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getDailyInterval() {
        return dailyInterval;
    }

    public void setDailyInterval(String dailyInterval) {
        this.dailyInterval = dailyInterval;
    }

    public int getTratamentPeriod() {
        return TratamentPeriod;
    }

    public void setTratamentPeriod(int tratamentPeriod) {
        TratamentPeriod = tratamentPeriod;
    }

    public List<Medication> getMedList() {
        return medList;
    }

    public void setMedList(List<Medication> medList) {
        this.medList = medList;
    }

}
