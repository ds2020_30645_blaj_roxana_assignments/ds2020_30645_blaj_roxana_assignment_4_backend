package com.example.sd_med.dtos;

public class PatientCaregiverDTO {

    private String patientName;
    private String caregiverName;

    public PatientCaregiverDTO() {
    }

    public PatientCaregiverDTO(String patientName, String caregiverName) {
        this.patientName = patientName;
        this.caregiverName = caregiverName;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getCaregiverName() {
        return caregiverName;
    }

    public void setCaregiverName(String caregiverName) {
        this.caregiverName = caregiverName;
    }

}
