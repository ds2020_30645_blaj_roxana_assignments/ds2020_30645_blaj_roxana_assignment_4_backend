package com.example.sd_med.dtos;

import com.example.sd_med.entities.Caregiver;
import com.example.sd_med.entities.MedicationPlan;
import org.springframework.hateoas.RepresentationModel;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Date;
import java.util.UUID;

public class PatientDTO extends RepresentationModel<MedicationDTO> {

    private UUID id;
    @NotNull
    @Pattern(regexp = "^[a-zA-Z0-9._ ]+$")
    private String name;
    @NotNull
    //@Pattern(regexp = "^(19|20)\\d\\d[-](0[1-9]|1[012])[-](0[1-9]|[12][0-9]|3[01])")
    private Date birthdate;
    @NotNull
    private String gender;
    @NotNull
    private String address;
    private String medicalRecord;
    private Caregiver caregiver;
    private MedicationPlan medPlan;


    public PatientDTO() {
    }

    public PatientDTO(UUID id, String name, Date birthdate, String gender, String address, String medicalRecord, Caregiver caregiver, MedicationPlan medPlan) {
        this.id = id;
        this.name = name;
        this.birthdate = birthdate;
        this.gender = gender;
        this.address = address;
        this.medicalRecord = medicalRecord;
        this.caregiver=caregiver;
        this.medPlan=medPlan;
    }

    public PatientDTO( String name, Date birthdate, String gender, String address, String medicalRecord, Caregiver caregiver,  MedicationPlan medPlan) {
        this.name = name;
        this.birthdate = birthdate;
        this.gender = gender;
        this.address = address;
        this.medicalRecord = medicalRecord;
        this.caregiver=caregiver;
        this.medPlan=medPlan;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

    public Caregiver getCaregiver() {
        return caregiver;
    }

    public void setCaregiver(Caregiver caregiver) {
        this.caregiver = caregiver;
    }

    public MedicationPlan getMedPlan() {
        return medPlan;
    }

    public void setMedPlan(MedicationPlan medPlan) {
        this.medPlan = medPlan;
    }
}
