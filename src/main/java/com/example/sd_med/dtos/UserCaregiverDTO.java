package com.example.sd_med.dtos;

public class UserCaregiverDTO {

    private UserDTO userDTO;
    private CaregiverDTO caregiverDTO;

    public UserCaregiverDTO() {
    }

    public UserCaregiverDTO(UserDTO userDTO, CaregiverDTO caregiverDTO) {
        this.userDTO = userDTO;
        this.caregiverDTO = caregiverDTO;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }

    public CaregiverDTO getCaregiverDTO() {
        return caregiverDTO;
    }

    public void setCaregiverDTO(CaregiverDTO caregiverDTO) {
        this.caregiverDTO = caregiverDTO;
    }
}
