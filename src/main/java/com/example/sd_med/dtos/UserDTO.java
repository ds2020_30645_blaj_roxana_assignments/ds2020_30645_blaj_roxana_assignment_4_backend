package com.example.sd_med.dtos;

import com.example.sd_med.dtos.validators.ROLE;
import com.example.sd_med.dtos.validators.annotation.UserRole;
import org.hibernate.annotations.ColumnTransformer;
import org.hibernate.validator.constraints.Length;
import org.springframework.hateoas.RepresentationModel;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.UUID;

public class UserDTO  extends RepresentationModel<UserDTO> {

    private UUID id;
    @NotNull
    @Length(min=6)
    @Pattern(regexp = "^[a-zA-Z0-9._]{6,20}$")
    private String username;
    @NotNull
    @Length(min=6)
    private String password;
    @NotNull
    @UserRole(enumClass = ROLE.class)
    private String role;

  /*  private Patient patient;
    private Caregiver caregiver;
    private Doctor doctor;*/

    public UserDTO() {
    }

    public UserDTO(UUID id, @NotNull String username, @NotNull String password, @NotNull String role){
        this.id=id;
        this.username = username;
        this.password = password;
        this.role = role;
    }

    public UserDTO(@NotNull String username, @NotNull String password, @NotNull String role) {
        this.username = username;
        this.password = password;
        this.role = role;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

}
