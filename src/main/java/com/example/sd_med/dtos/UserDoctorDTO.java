package com.example.sd_med.dtos;

public class UserDoctorDTO {

    public UserDTO userDTO;
    public DoctorDTO doctorDTO;

    public UserDoctorDTO(UserDTO userDTO, DoctorDTO doctorDTO) {
        this.userDTO = userDTO;
        this.doctorDTO = doctorDTO;
    }

    public UserDoctorDTO() {
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }

    public DoctorDTO getDoctorDTO() {
        return doctorDTO;
    }

    public void setDoctorDTO(DoctorDTO doctorDTO) {
        this.doctorDTO = doctorDTO;
    }
}
