package com.example.sd_med.dtos;

public class UserPatientDTO {

    private UserDTO userDTO;
    private PatientDTO patientDTO;

    public UserPatientDTO() {
    }

    public UserPatientDTO(UserDTO userDTO, PatientDTO patientDTO) {
        this.userDTO = userDTO;
        this.patientDTO = patientDTO;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }

    public PatientDTO getPatientDTO() {
        return patientDTO;
    }

    public void setPatientDTO(PatientDTO patientDTO) {
        this.patientDTO = patientDTO;
    }
}
