package com.example.sd_med.dtos.builder;

import com.example.sd_med.dtos.CaregiverDTO;
import com.example.sd_med.dtos.PatientDTO;
import com.example.sd_med.entities.Caregiver;
import com.example.sd_med.entities.Patient;

import java.util.ArrayList;
import java.util.List;

public class CaregiverBuilder {


    public CaregiverBuilder() {
    }

    public static CaregiverDTO toCaregiverDTO(Caregiver caregiver){
        //List<PatientDTO> patients = new ArrayList<PatientDTO>();
      /*  if(caregiver.getPatients()!=null){
         for(Patient patient:caregiver.getPatients()){
            patients.add(PatientBuilder.toPatientDTO(patient));
        }}*/

        return new CaregiverDTO(caregiver.getId(),
                caregiver.getName(),
                caregiver.getBirthdate(),
                caregiver.getGender(),
                caregiver.getAddress()//,
                // patients
        );
    }

    public static Caregiver toEntity(CaregiverDTO caregiverDTO){
        //  ArrayList<Patient> patients =new ArrayList<Patient>();

       /* if(caregiverDTO.getPatients()!=null){
            for(PatientDTO patientDTO: caregiverDTO.getPatients()){
                patients.add(PatientBuilder.toEntity(patientDTO));
            }
        }
*/
        return new Caregiver(
                caregiverDTO.getName(),
                caregiverDTO.getBirthdate(),
                caregiverDTO.getGender(),
                caregiverDTO.getAddress()
        );
    }


}
