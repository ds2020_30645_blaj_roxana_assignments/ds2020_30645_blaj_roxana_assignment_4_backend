package com.example.sd_med.dtos.builder;

import com.example.sd_med.dtos.DoctorDTO;
import com.example.sd_med.entities.Doctor;

public class DoctorBuilder {

    public DoctorBuilder() {
    }

    public static DoctorDTO toDoctorDTO(Doctor doctor) {
        return new DoctorDTO(
                doctor.getId(),
                doctor.getName(),
                doctor.getSpecialization());
    }

    public static Doctor toEntity(DoctorDTO doctorDTO) {
        return new Doctor(
                doctorDTO.getName(),
                doctorDTO.getSpecialization());
    }

}
