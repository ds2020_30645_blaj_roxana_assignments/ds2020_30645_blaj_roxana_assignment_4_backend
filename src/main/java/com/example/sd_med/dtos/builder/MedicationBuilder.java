package com.example.sd_med.dtos.builder;

import com.example.sd_med.dtos.MedicationDTO;
import com.example.sd_med.entities.Medication;

public class MedicationBuilder {

    public MedicationBuilder() {
    }

    public static MedicationDTO toMedicationDTO(Medication medication) {
        return new MedicationDTO(medication.getId(), medication.getName(), medication.getSideEffects(), medication.getDosage());
    }

    public static Medication toEntity(MedicationDTO medicationDTO) {
        return new Medication(medicationDTO.getName(),
                medicationDTO.getSideEffects(),
                medicationDTO.getDosage());
    }

}
