package com.example.sd_med.dtos.builder;

import com.example.sd_med.dtos.MedicationPlanDTO;
import com.example.sd_med.entities.MedicationPlan;

public class MedicationPlanBuilder {
    private MedicationPlanBuilder() {
    }

    public static MedicationPlanDTO toMedicationPlanDTO(MedicationPlan medicationPlan) {
        return new MedicationPlanDTO(medicationPlan.getId(),
                medicationPlan.getDailyInterval(),
                medicationPlan.getTratamentPeriod(),
                medicationPlan.getMedList()
        );
    }


    public static MedicationPlan toEntity(MedicationPlanDTO medicationPlanDTO) {
        return new MedicationPlan(medicationPlanDTO.getId(),
                medicationPlanDTO.getDailyInterval(),
                medicationPlanDTO.getTratamentPeriod(),
                medicationPlanDTO.getMedList());
    }

}
