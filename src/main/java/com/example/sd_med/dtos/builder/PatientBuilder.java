package com.example.sd_med.dtos.builder;

import com.example.sd_med.dtos.PatientDTO;
import com.example.sd_med.entities.Patient;

public class PatientBuilder {

    public PatientBuilder() {
    }

    public static PatientDTO toPatientDTO(Patient patient){
        return new PatientDTO(patient.getId(),
                patient.getName(),
                patient.getBirthdate(),
                patient.getGender(),
                patient.getAddress(),
                patient.getMedicalRecord(),
                patient.getCaregiver(),
                patient.getMedPlan());
    }

    public static Patient toEntity(PatientDTO patientDTO){
        return new Patient(
                patientDTO.getName(),
                patientDTO.getBirthdate(),
                patientDTO.getGender(),
                patientDTO.getAddress(),
                patientDTO.getMedicalRecord(),
                patientDTO.getCaregiver(),
                patientDTO.getMedPlan());
    }

}
