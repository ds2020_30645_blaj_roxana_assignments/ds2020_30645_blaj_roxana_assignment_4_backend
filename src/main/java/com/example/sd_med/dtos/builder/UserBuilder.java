package com.example.sd_med.dtos.builder;

import com.example.sd_med.dtos.UserDTO;
import com.example.sd_med.entities.User;

public class UserBuilder {

    private UserBuilder() {
    }

    public static UserDTO toUserDTO(User user) {
        return new UserDTO(
                user.getId(),
                user.getUsername(),
                user.getPassword(),
                user.getUserRole() );
    }

    public static User toEntity(UserDTO userDTO) {
        return new User( userDTO.getUsername(),
                userDTO.getPassword(),
                userDTO.getRole());
    }
}
