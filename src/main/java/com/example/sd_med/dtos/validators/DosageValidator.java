package com.example.sd_med.dtos.validators;

import com.example.sd_med.dtos.validators.annotation.DosageLimit;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class DosageValidator  implements ConstraintValidator<DosageLimit, Double> {

    private double dosageLimit;

    @Override
    public void initialize(DosageLimit constraintAnnotation) {
        this.dosageLimit = constraintAnnotation.limit();
    }

    @Override
    public boolean isValid(Double value, ConstraintValidatorContext context) {
        return value <= dosageLimit && value > 0;
    }


}
