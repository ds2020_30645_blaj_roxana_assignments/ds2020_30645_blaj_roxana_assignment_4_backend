package com.example.sd_med.dtos.validators;

public enum ROLE {

    DOCTOR,
    CAREGIVER,
    PATIENT

}
