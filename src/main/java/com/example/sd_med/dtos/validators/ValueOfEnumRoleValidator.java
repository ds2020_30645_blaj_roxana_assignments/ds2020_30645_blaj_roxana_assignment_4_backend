package com.example.sd_med.dtos.validators;

import com.example.sd_med.dtos.validators.annotation.UserRole;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ValueOfEnumRoleValidator implements ConstraintValidator<UserRole, CharSequence> {

    private List<String> acceptedValues;

    @Override
    public void initialize(UserRole constraintAnnotation) {
        acceptedValues = Stream.of(constraintAnnotation.enumClass().getEnumConstants())
                .map(Enum::name)
                .collect(Collectors.toList());
    }

    @Override
    public boolean isValid(CharSequence value, ConstraintValidatorContext context) {
        return acceptedValues.contains(value.toString());
    }
}
