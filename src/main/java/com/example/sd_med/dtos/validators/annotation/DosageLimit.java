package com.example.sd_med.dtos.validators.annotation;

import com.example.sd_med.dtos.validators.DosageValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {DosageValidator.class})
public @interface DosageLimit {

    double limit() default 700.0;

    String message() default "Dosage does not match the required limit";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};


}
