package com.example.sd_med.entities;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.UUID;

@Entity
public class Doctor implements Serializable {

    public Doctor(){}

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID id;

    @OneToOne
    @MapsId
    private User user;

    @Column(name = "name", nullable = false)
    @Pattern(regexp = "^[a-zA-Z0-9._ ]+$")
    private String name;

    @Column(name="specialization", nullable=false)
    @Pattern(regexp="^[A-Za-z]+$")
    private String specialization;

    public Doctor(User user,String name, String specialization){
        this.user=user;
        this.name=name;
        this.specialization=specialization;
    }

    public Doctor(String name, String specialization){
        this.name=name;
        this.specialization=specialization;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

}
