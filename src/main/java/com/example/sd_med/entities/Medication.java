package com.example.sd_med.entities;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.UUID;

@Entity
public class Medication implements Serializable {

    public Medication() {}

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID id;

    @Column(name="name", nullable=false)
    @Pattern(regexp = "^[a-zA-Z0-9,.]+$")
    private String name;

    @Column(name="sideEffects", nullable=false)

    private String sideEffects;

    @Column(name="dosage", nullable=false)
    private double dosage;

    /*@ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "medicationPlan_id")
    private MedicationPlan medicationPlan;*/

    public Medication(String name, String sideEffects, double dosage){
        this.name=name;
        this.sideEffects=sideEffects;
        this.dosage=dosage;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSideEffects() {
        return sideEffects;
    }

    public void setSideEffects(String sideEffects) {
        this.sideEffects = sideEffects;
    }

    public double getDosage() {
        return dosage;
    }

    public void setDosage(double dosage) {
        this.dosage = dosage;
    }
}
