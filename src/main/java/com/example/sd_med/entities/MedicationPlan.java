package com.example.sd_med.entities;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;

@Entity
public class MedicationPlan implements Serializable {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID id;

    @Column(name = "dailyInterval")
    @Pattern(regexp="^[0-2]-[0-2]-[0-2]$")
    private String dailyInterval;

    @Column(name = "TratamentPeriod")
    private int TratamentPeriod;


    @ManyToMany(cascade=CascadeType.MERGE, fetch=FetchType.EAGER)
    private List<Medication> medList;

    public MedicationPlan(){}

    public MedicationPlan(UUID id, String dailyInterval, int TratamentPeriod, List<Medication> medList){
        this.id=id;
        this.dailyInterval=dailyInterval;
        this.TratamentPeriod=TratamentPeriod;
        this.medList=medList;
    }

    public MedicationPlan(String dailyInterval, int TratamentPeriod, List<Medication> medList){
        this.dailyInterval=dailyInterval;
        this.TratamentPeriod=TratamentPeriod;
        this.medList=medList;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getDailyInterval() {
        return dailyInterval;
    }

    public void setDailyInterval(String dailyInterval) {
        this.dailyInterval = dailyInterval;
    }

    public int getTratamentPeriod() {
        return TratamentPeriod;
    }

    public void setTratamentPeriod(int tratamentPeriod) {
        TratamentPeriod = tratamentPeriod;
    }

    public List<Medication> getMedList() {
        return medList;
    }

    public void setMedList(List<Medication> medList) {
        this.medList = medList;
    }

}
