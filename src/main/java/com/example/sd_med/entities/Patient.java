package com.example.sd_med.entities;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Entity
public class Patient implements Serializable {

    public Patient(){}

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID id;

    @OneToOne
    @MapsId
    private User user;

    @Column(name = "name", nullable = false)
    @Pattern(regexp = "^[a-zA-Z0-9._ ]+$")
    private String name;

    @Column(name = "birthdate")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date birthdate;

    @Column(name = "gender", nullable = false)
    @Pattern(regexp = "^[A-Z]+$")
    private String gender;

    @Column(name = "address", nullable = false)
    @Pattern(regexp = "^[a-zA-Z0-9., ]+$")
    private String address;


    @Column(name = "medicalRecord")
    @Pattern(regexp = "^[a-zA-Z0-9,. ]+$")
    private String medicalRecord;

    @ManyToOne(fetch = FetchType.EAGER)
    private Caregiver caregiver;

    @OneToOne(cascade=CascadeType.MERGE, fetch=FetchType.EAGER)
    private MedicationPlan medPlan;

    public Patient( String name, Date birthdate,  String gender,String address, String medicalRecord, Caregiver caregiver, MedicationPlan medPlan) {
        this.name = name;
        this.birthdate = birthdate;
        this.gender = gender;
        this.address = address;
        this.medicalRecord=medicalRecord;
        this.caregiver=caregiver;
        this.medPlan=medPlan;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

    public Caregiver getCaregiver() {
        return caregiver;
    }

    public void setCaregiver(Caregiver caregiver) {
        this.caregiver = caregiver;
    }

    public MedicationPlan getMedPlan() {
        return medPlan;
    }

    public void setMedPlan(MedicationPlan medPlan) {
        this.medPlan = medPlan;
    }
}
