package com.example.sd_med.entities;

import org.hibernate.annotations.ColumnTransformer;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.UUID;

@Entity
@Table(name = "userr")
public class User implements Serializable {


    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID id;

    @Column(name = "username", unique = true, nullable = false)
    @Pattern(regexp = "^[a-zA-Z0-9._]{6,20}$")
    private String username;

    @Column(name="password", nullable=false)
    //@Pattern(regexp = "^[a-zA-Z0-9._@&^$#-]{6,20}$")
    //@ColumnTransformer( write = "PGP_SYM_ENCRYPT(?, 'db_medical')")
    private String password;

    @Column(name = "role", nullable = false)
    @Pattern(regexp = "^[A-Z_]+$")
    private String role;

    @OneToOne(mappedBy = "user", cascade = CascadeType.MERGE, orphanRemoval = true)
    private Patient patient;

    @OneToOne(mappedBy = "user", cascade = CascadeType.MERGE, orphanRemoval = true)
    private Caregiver caregiver;

    @OneToOne(mappedBy = "user", cascade = CascadeType.MERGE, orphanRemoval = true)
    private Doctor doctor;

    public User(){}

    public User(String username, String password, String userRole) {
        this.username = username;
        this.password = password;
        this.role = userRole;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return  password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserRole() {
        return role;
    }

    public void setUserRole(String userRole) {
        this.role = userRole;
    }

    public void addDoctor(Doctor doctor){
        this.doctor=doctor;
        doctor.setUser(this);
    }
    public void addCaregiver(Caregiver caregiver){
        this.caregiver=caregiver;
        caregiver.setUser(this);
    }
    public void addPatient(Patient patient){
        this.patient=patient;
        patient.setUser(this);
    }

}
