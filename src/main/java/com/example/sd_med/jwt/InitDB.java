package com.example.sd_med.jwt;

import com.example.sd_med.entities.User;
import com.example.sd_med.repositories.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

//@Service
public class InitDB implements CommandLineRunner {

    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;

    public InitDB(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void run(String... args) {

        userRepository.deleteAll();
        // Crete users
        //User p = new User("patientTest", passwordEncoder.encode("1234567"),"PATIENT");
        //User c = new User("caregiverTest", passwordEncoder.encode("1234567"),"CAREGIVER");
        User d = new User("doctorTest1", passwordEncoder.encode("1234567"),"DOCTOR");

        List<User> users = Arrays.asList(d);

        // Save to db
        userRepository.saveAll(users);
    }
}