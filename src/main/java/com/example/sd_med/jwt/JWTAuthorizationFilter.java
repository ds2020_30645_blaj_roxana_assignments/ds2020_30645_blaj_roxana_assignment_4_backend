package com.example.sd_med.jwt;

import io.jsonwebtoken.Jwts;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static java.util.Collections.emptyList;

public class JWTAuthorizationFilter  extends BasicAuthenticationFilter {

    public JWTAuthorizationFilter(AuthenticationManager authenticationManager) {
        super(authenticationManager);
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        String header = request.getHeader(JWTConstants.HEADER);
        if (header == null || !header.startsWith(JWTConstants.TOKEN_PREFIX)) {
            chain.doFilter(request, response);
        } else {
            Authentication authentication = getAuthentication(request);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            chain.doFilter(request, response);
        }
    }

    private Authentication getAuthentication(HttpServletRequest request) {
        //System.out.println("REQUEST -->  "+request);
        String header = request.getHeader(JWTConstants.HEADER);
        //System.out.println(header.replace(JWTConstants.TOKEN_PREFIX, ""));
        if (header != null) {
            System.out.println("HEADER -->  "+ header );
            String user =  Jwts.parser()
                    .setSigningKey(JWTConstants.SECRET_KEY)
                    .parseClaimsJws(header.replace(JWTConstants.TOKEN_PREFIX, ""))
                    .getBody()
                    .getSubject();
            //System.out.println(user + this.getClass().getSimpleName());
            if (user != null) {
                return new UsernamePasswordAuthenticationToken(user, null, emptyList());
            }
        }
        return null;
    }


}
