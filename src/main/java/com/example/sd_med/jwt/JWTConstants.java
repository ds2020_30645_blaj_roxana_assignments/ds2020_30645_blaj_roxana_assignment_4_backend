package com.example.sd_med.jwt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;

public class JWTConstants {

    public static final long EXPIRATION_TIME = 86400000L; //1day
    public static final String SECRET_KEY =  "secret@Key";
    public static final String HEADER = "Authorization";
    public static final String TOKEN_PREFIX = "Bearer ";

}
