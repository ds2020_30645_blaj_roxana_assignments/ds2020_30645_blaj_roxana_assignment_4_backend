package com.example.sd_med.repositories;

import com.example.sd_med.entities.Caregiver;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface CaregiverRepository extends JpaRepository<Caregiver, UUID> {
    Optional<Caregiver> findByName(String name);
}
