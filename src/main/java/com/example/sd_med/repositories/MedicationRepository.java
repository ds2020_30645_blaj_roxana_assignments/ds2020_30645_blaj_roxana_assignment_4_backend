package com.example.sd_med.repositories;

import com.example.sd_med.entities.Medication;
import com.example.sd_med.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface MedicationRepository extends JpaRepository<Medication, UUID> {
    Optional<Medication> findByName(String name);
}
