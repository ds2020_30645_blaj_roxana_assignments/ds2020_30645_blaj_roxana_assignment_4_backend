package com.example.sd_med.repositories;

import com.example.sd_med.dtos.CaregiverDTO;
import com.example.sd_med.dtos.SCaregiverDTO;
import com.example.sd_med.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {
    public User findByUsername(String username);

    @Query(value = "SELECT pgp_sym_decrypt(password\\:\\:bytea, 'db_medical') FROM Userr WHERE username = :username ",
            nativeQuery = true)
    public String findDecryptPass(@Param("username") String username);

    @Query(value="SELECT cg.name\n" +
            "FROM Userr u INNER JOIN Caregiver cg\n" +
            "ON u.id=cg.user_id AND u.username= :username", nativeQuery = true)
    public String findCaregiver(@Param("username") String username);

    @Query(value="SELECT pt.name\n" +
            "FROM Userr u INNER JOIN Patient pt\n" +
            "ON u.id=pt.user_id AND u.username= :username", nativeQuery = true)
    public String findPatient(@Param("username") String username);

  /*  @Query(value="SELECT pt.user_id\n" +
            "FROM Userr u INNER JOIN Patient pt\n" +
            "ON u.id=pt.user_id AND u.username= :usernameP", nativeQuery = true)
    public UUID findPatientId(@Param("usernameP") String usernameP);*/

    @Query(value="SELECT cg.user_id\n" +
            "FROM Userr u INNER JOIN Caregiver cg\n" +
            "ON u.id=cg.user_id AND u.username= :username", nativeQuery = true)
    public UUID findCaregiverId(@Param("username") String username);

}
