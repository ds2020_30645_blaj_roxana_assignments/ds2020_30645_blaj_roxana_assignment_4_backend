package com.example.sd_med.services;

import com.example.sd_med.entities.User;
import com.example.sd_med.repositories.UserRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AuthService implements UserDetailsService {

    private final UserRepository userRepository;

    public AuthService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(s);
        if (user == null)
            throw new UsernameNotFoundException("User not found");

        List<GrantedAuthority> authoritites = new ArrayList<>();

        GrantedAuthority grantedAuthority = new GrantedAuthority() {
            @Override
            public String getAuthority() {
                return   user.getUserRole();
            }
        };

        authoritites.add(grantedAuthority);
        System.out.println(grantedAuthority.getAuthority());

        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), authoritites);
    }
}
