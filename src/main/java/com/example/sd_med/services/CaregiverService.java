package com.example.sd_med.services;

import com.example.sd_med.controllers.handlers.exceptions.model.ResourceNotFoundException;
import com.example.sd_med.dtos.CaregiverDTO;
import com.example.sd_med.dtos.builder.CaregiverBuilder;
import com.example.sd_med.entities.Caregiver;
import com.example.sd_med.entities.Patient;
import com.example.sd_med.repositories.CaregiverRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class CaregiverService {


    private static final Logger LOGGER = LoggerFactory.getLogger(PatientService.class);
    private final CaregiverRepository caregiverRepository;

    @Autowired
    public CaregiverService(CaregiverRepository caregiverRepository) {
        this.caregiverRepository = caregiverRepository;
    }

    public List<CaregiverDTO> findCaregivers() {
        List<Caregiver> caregiverList = caregiverRepository.findAll();
        return caregiverList.stream()
                .map(CaregiverBuilder::toCaregiverDTO)
                .collect(Collectors.toList());
    }

    public CaregiverDTO findCaregiverById(UUID id) {
        Optional<Caregiver> prosumerOptional = caregiverRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Caregiver with id {} was not found in db", id);
            throw new ResourceNotFoundException(Patient.class.getSimpleName() + " with ID: " + id);
        }
        return CaregiverBuilder.toCaregiverDTO(prosumerOptional.get());
    }

    public CaregiverDTO findCaregiverByName(String name) {
        Optional<Caregiver> prosumerOptional = caregiverRepository.findByName(name);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Patient with name {} was not found in db", name);
            throw new ResourceNotFoundException(Patient.class.getSimpleName() + " with name: " + name);
        }
        return CaregiverBuilder.toCaregiverDTO(prosumerOptional.get());
    }

    public CaregiverDTO updateCaregiver(String name, CaregiverDTO caregiverDTO){

        Caregiver c = caregiverRepository.findByName(name).get();
        c.setName(caregiverDTO.getName());
        c.setAddress(caregiverDTO.getAddress());
        c.setBirthdate(c.getBirthdate());
        c.setGender(caregiverDTO.getGender());

        caregiverRepository.save(c);
        CaregiverDTO caregiver = CaregiverBuilder.toCaregiverDTO(c);
        return caregiver;
    }


   /* public CaregiverDTO updateCaregiver(String name, CaregiverDTO caregiverDTO){

        Caregiver c = caregiverRepository.findByName(name).get();
        c.setName(caregiverDTO.getName());
        c.setAddress(caregiverDTO.getAddress());
        c.setGender(caregiverDTO.getGender());

        ArrayList<Patient> patients =new ArrayList<Patient>();

        if(caregiverDTO.getPatients()!=null){
            for(PatientDTO patientDTO: caregiverDTO.getPatients()){
                patients.add(PatientBuilder.toEntity(patientDTO));
            }
        }
        c.setPatients(patients);
        caregiverRepository.save(c);

        CaregiverDTO caregiver = CaregiverBuilder.toCaregiverDTO(c);
        return caregiver;
    }
*/



}
