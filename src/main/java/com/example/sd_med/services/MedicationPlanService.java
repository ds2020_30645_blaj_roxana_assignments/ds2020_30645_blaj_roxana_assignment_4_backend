package com.example.sd_med.services;

import com.example.sd_med.controllers.handlers.exceptions.model.ResourceNotFoundException;
import com.example.sd_med.dtos.MedicationPlanDTO;
import com.example.sd_med.dtos.builder.MedicationPlanBuilder;
import com.example.sd_med.entities.Medication;
import com.example.sd_med.entities.MedicationPlan;
import com.example.sd_med.repositories.MedicationPlanRepository;
import com.example.sd_med.repositories.MedicationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class MedicationPlanService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MedicationPlanService.class);
    private final MedicationPlanRepository medicationPlanRepository;
    private final MedicationRepository medicationRepository;

    @Autowired
    public MedicationPlanService(MedicationPlanRepository medicationPlanRepository, MedicationRepository medicationRepository) {
        this.medicationPlanRepository = medicationPlanRepository;
        this.medicationRepository = medicationRepository;
    }

    public MedicationPlanDTO getMedPlanByID(UUID id) {
        Optional<MedicationPlan> prosumerOptional = medicationPlanRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Medication plan  with id {} was not found in db", id);
            throw new ResourceNotFoundException(MedicationPlan.class.getSimpleName() + " with id: " + id);
        }
        return MedicationPlanBuilder.toMedicationPlanDTO(prosumerOptional.get());
    }

    public List<MedicationPlanDTO> getMedicationPlans(){
        List<MedicationPlan> medicationsList = medicationPlanRepository.findAll();
        return medicationsList.stream()
                .map(MedicationPlanBuilder::toMedicationPlanDTO)
                .collect(Collectors.toList());
    }

    public MedicationPlan addMedicationPlan(MedicationPlanDTO medicationPlanDTO) {
        MedicationPlan medPlan = MedicationPlanBuilder.toEntity(medicationPlanDTO);
        List<Medication> medList = medPlan.getMedList();
        List<Medication> newMedList= new ArrayList<Medication>();
        for(Medication m: medList){
            Optional<Medication> findMedication = medicationRepository.findByName(m.getName());
            Medication med = findMedication.get();
            newMedList.add(med);
        }
        medPlan.setMedList(newMedList);
        medPlan = medicationPlanRepository.save(medPlan);
        LOGGER.debug("Medication plan with id {} was inserted in db", medPlan.getId());
        return medPlan;
    }

    public void deleteMedPlanByID(UUID id)
    {
        Optional<MedicationPlan> prosumerOptional = medicationPlanRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Medication plan with id {} was not found in db", id);
            throw new ResourceNotFoundException(MedicationPlan.class.getSimpleName() + " with id: " + id+" was not found!");
        }
        medicationPlanRepository.deleteById(id);
    }
}
