package com.example.sd_med.services;

import com.example.sd_med.dtos.MedicationDTO;
import com.example.sd_med.dtos.builder.MedicationBuilder;
import com.example.sd_med.entities.Medication;
import com.example.sd_med.entities.User;
import com.example.sd_med.repositories.MedicationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class MedicationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MedicationService.class);

    private final MedicationRepository medicationRepository;

    @Autowired
    public MedicationService(MedicationRepository medicationRepository){this.medicationRepository=medicationRepository;}

    /**
     * Create medication
     */
    public UUID insert(MedicationDTO medicationDTO){
        Medication medication = MedicationBuilder.toEntity(medicationDTO);
        medication = medicationRepository.save(medication);
        LOGGER.debug("Medication with id {} was inserted in db", medication.getId());
        return medication.getId();
    }

    /**
     * Delete medication
     */
    public void deleteMedicationById(UUID id){
        medicationRepository.deleteById(id);
    }

    public UUID deleteMedicationByName(String name){
        Medication medication = medicationRepository.findByName(name).get();

        medicationRepository.deleteById(medication.getId());
        LOGGER.debug("Medication with id {} was deleted in db", medication.getId());
        return medication.getId();

    }
    /**
     * Update medication
     */
    public MedicationDTO updateMedication(UUID id, MedicationDTO medicationDTO){
        Medication medication=medicationRepository.findById(id).get();
        medication.setName(medicationDTO.getName());
        medication.setSideEffects(medicationDTO.getSideEffects());
        medication.setDosage(medicationDTO.getDosage());
        medicationRepository.save(medication);
        MedicationDTO mDTO = MedicationBuilder.toMedicationDTO(medication);
        return mDTO;
    }

    public UUID updateMedicationByName(String name, MedicationDTO medicationDTO){
        Medication medication=medicationRepository.findByName(name).get();
        medication.setName(medicationDTO.getName());
        medication.setSideEffects(medicationDTO.getSideEffects());
        medication.setDosage(medicationDTO.getDosage());
        medicationRepository.save(medication);
        //MedicationDTO mDTO = MedicationBuilder.toMedicationDTO(medication);
        return medication.getId();
    }

    /**
     * View all medication & medication by id
     */
    public List<MedicationDTO> findMedications() {
        List<Medication> medicationList = medicationRepository.findAll();
        return medicationList.stream()
                .map(MedicationBuilder::toMedicationDTO)
                .collect(Collectors.toList());
    }

    public MedicationDTO findMedicationById(UUID id) {
        Optional<Medication> prosumerOptional = medicationRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Medication with id {} was not found in db", id);
            throw new ResourceNotFoundException(Medication.class.getSimpleName() + " with id: " + id);
        }
        return MedicationBuilder.toMedicationDTO(prosumerOptional.get());
    }

}
