package com.example.sd_med.services;

import com.example.sd_med.controllers.handlers.exceptions.model.ResourceNotFoundException;
import com.example.sd_med.dtos.MedicalRecordDTO;
import com.example.sd_med.dtos.PatientDTO;
import com.example.sd_med.dtos.builder.PatientBuilder;
import com.example.sd_med.entities.MedicationPlan;
import com.example.sd_med.entities.Patient;
import com.example.sd_med.repositories.MedicationPlanRepository;
import com.example.sd_med.repositories.PatientRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class PatientService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PatientService.class);
    private final PatientRepository patientRepository;
    private final MedicationPlanRepository medicationPlanRepository;

    @Autowired
    public PatientService(PatientRepository patientRepository, MedicationPlanRepository medicationPlanRepository) {
        this.patientRepository = patientRepository;
        this.medicationPlanRepository = medicationPlanRepository;
    }

    public List<PatientDTO> findPatient() {
        List<Patient> patientList = patientRepository.findAll();
        return patientList.stream()
                .map(PatientBuilder::toPatientDTO)
                .collect(Collectors.toList());
    }

    public PatientDTO findPatientById(UUID id) {
        Optional<Patient> prosumerOptional = patientRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Patient with id {} was not found in db", id);
            throw new ResourceNotFoundException(Patient.class.getSimpleName() + " with ID: " + id);
        }
        return PatientBuilder.toPatientDTO(prosumerOptional.get());
    }

    public PatientDTO findPatientByName(String name) {
        Optional<Patient> prosumerOptional = patientRepository.findByName(name);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Patient with name {} was not found in db", name);
            throw new ResourceNotFoundException(Patient.class.getSimpleName() + " with name: " + name);
        }
        return PatientBuilder.toPatientDTO(prosumerOptional.get());
    }

    public PatientDTO updatePatient(String name, PatientDTO patientDTO){

        Patient p = patientRepository.findByName(name).get();
        p.setName(patientDTO.getName());
        p.setAddress(patientDTO.getAddress());
        p.setBirthdate(p.getBirthdate());
        p.setGender(patientDTO.getGender());
        p.setMedicalRecord(p.getMedicalRecord());
        patientRepository.save(p);
        PatientDTO person = PatientBuilder.toPatientDTO(p);
        return person;
    }


    public PatientDTO updateMedicalRecord(String name, MedicalRecordDTO medicalRecord){
        Patient p = patientRepository.findByName(name).get();
        p.setMedicalRecord(medicalRecord.getMedicalRecord());
        patientRepository.save(p);
        PatientDTO person = PatientBuilder.toPatientDTO(p);
        return person;
    }

    //add
    public PatientDTO updateMedPlan(String name, UUID id){
        Patient p = patientRepository.findByName(name).get();
        System.out.println(p.getId());
        System.out.println(p.getBirthdate());
        System.out.println(p.getCaregiver());
        System.out.println(p.getMedicalRecord());
        MedicationPlan medicationPlan = medicationPlanRepository.findById(id).get();
        p.setMedPlan(medicationPlan);
        System.out.println(medicationPlan.getTratamentPeriod() + " " + medicationPlan.getDailyInterval());
        // medicationPlanRepository.save(p.getMedPlan());
        patientRepository.save(p);
        PatientDTO person = PatientBuilder.toPatientDTO(p);
        return person;
    }



}
