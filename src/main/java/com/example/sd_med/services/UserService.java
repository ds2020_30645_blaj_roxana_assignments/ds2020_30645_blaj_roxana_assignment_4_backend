package com.example.sd_med.services;

import com.example.sd_med.controllers.handlers.exceptions.model.EntityValidationException;
import com.example.sd_med.controllers.handlers.exceptions.model.ResourceNotFoundException;
import com.example.sd_med.dtos.*;
import com.example.sd_med.dtos.builder.CaregiverBuilder;
import com.example.sd_med.dtos.builder.DoctorBuilder;
import com.example.sd_med.dtos.builder.PatientBuilder;
import com.example.sd_med.dtos.builder.UserBuilder;
import com.example.sd_med.entities.Caregiver;
import com.example.sd_med.entities.Doctor;
import com.example.sd_med.entities.Patient;
import com.example.sd_med.entities.User;
import com.example.sd_med.repositories.CaregiverRepository;
import com.example.sd_med.repositories.DoctorRepository;
import com.example.sd_med.repositories.PatientRepository;
import com.example.sd_med.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);
    private final UserRepository userRepository;
    private final DoctorRepository doctorRepository;
    private final CaregiverRepository caregiverRepository;
    private final PatientRepository patientRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserService(UserRepository userRepository, DoctorRepository doctorRepository,
                       CaregiverRepository caregiverRepository, PatientRepository patientRepository, BCryptPasswordEncoder bCryptPasswordEncoder){
        this.userRepository=userRepository;
        this.doctorRepository=doctorRepository;
        this.caregiverRepository=caregiverRepository;
        this.patientRepository=patientRepository;
        this.bCryptPasswordEncoder=bCryptPasswordEncoder;

    }



    public UUID createUser(UserDTO userDTO) {
        User user = UserBuilder.toEntity(userDTO);
        System.out.println();
        System.out.println("role"+user.getUserRole());
        System.out.println("username"+user.getUsername());
        user = userRepository.save(user);

        return user.getId();
    }

    public List<UserDTO> findUser() {
        List<User> UserList = userRepository.findAll();
        return UserList.stream()
                .map(UserBuilder::toUserDTO)
                .collect(Collectors.toList());
    }

    public UserDTO findUserById(UUID id) {
        Optional<User> userOptional = userRepository.findById(id);
        if (!userOptional.isPresent()) {
            LOGGER.error("Person with id {} was not found in db", id);
            throw new ResourceNotFoundException(User.class.getSimpleName() + " with id: " + id);
        }
        return UserBuilder.toUserDTO(userOptional.get());
    }

    public void deleteUserById(UUID id){
        userRepository.deleteById(id);
    }

    public UUID deleteUserByUsername(String username){
        User user = userRepository.findByUsername(username);

        LOGGER.debug("User with id {} was deleted in db", user.getId());
        userRepository.deleteById(user.getId());

        return user.getId();
    }

    public UUID createUserDoctor(UserDoctorDTO userDoctorDTO) {
        User user = UserBuilder.toEntity(userDoctorDTO.getUserDTO());
        user.setPassword(bCryptPasswordEncoder.encode(userDoctorDTO.getUserDTO().getPassword()));
        if(user.getUserRole().equals("DOCTOR")) {
           Doctor d= DoctorBuilder.toEntity(userDoctorDTO.getDoctorDTO());

           LOGGER.debug("Doctor with id - created!", d.getId());
           user.addDoctor(d);
           d=doctorRepository.save(d);
        }

        user = userRepository.save(user);
        return user.getId();
    }

    public UUID createUserPatient(UserPatientDTO userPatientDTO) {
        User user = UserBuilder.toEntity(userPatientDTO.getUserDTO());
        user.setPassword(bCryptPasswordEncoder.encode(userPatientDTO.getUserDTO().getPassword()));
        if(user.getUserRole().equals("PATIENT")) {
            Patient p= PatientBuilder.toEntity(userPatientDTO.getPatientDTO());
            Caregiver c=p.getCaregiver();
            System.out.println(c.getName());// find caregiver with this name
            Caregiver ci= findCaregiverByName(c.getName());
            System.out.println(ci.getId());
            p.setCaregiver(ci);
            LOGGER.debug("Patient with id - created!", p.getId());
            user.addPatient(p);
            p=patientRepository.save(p);
        }

        user = userRepository.save(user);
        return user.getId();
    }


    public Caregiver findCaregiverByName(String name) {
        Optional<Caregiver> prosumerOptional = caregiverRepository.findByName(name);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Patient with name {} was not found in db", name);
            throw new ResourceNotFoundException(Patient.class.getSimpleName() + " with name: " + name);
        }
        return prosumerOptional.get();
    }


    public UUID createUserCaregiver(UserCaregiverDTO userCaregiverDTO) {
        User user = UserBuilder.toEntity(userCaregiverDTO.getUserDTO());
        user.setPassword(bCryptPasswordEncoder.encode(userCaregiverDTO.getUserDTO().getPassword()));
        if(user.getUserRole().equals("CAREGIVER")) {

            Caregiver c= CaregiverBuilder.toEntity(userCaregiverDTO.getCaregiverDTO());
            //List<Patient> p = c.getPatients();
            //c.setPatients(p);
            user.addCaregiver(c);
            c=caregiverRepository.save(c);

        }
        user = userRepository.save(user);
        return user.getId();
    }

    public UserDTO login(LoginInfoDTO loginInfoDTO){
        System.out.println(loginInfoDTO.getUsername());
        if(userRepository.findByUsername(loginInfoDTO.getUsername())!=null) {
            User user = userRepository.findByUsername(loginInfoDTO.getUsername());
            if((loginInfoDTO.getPassword()).equals(userRepository.findDecryptPass(user.getUsername()))) {
                System.out.println("Login as -->  "+ user.getUserRole());
                return UserBuilder.toUserDTO(user);
            }
        }

        throw new EntityValidationException(User.class.getSimpleName(), Collections.singletonList("Unknown user"));
    }

    public String getCaregiverWithUsername(String username){
        String caregiver = userRepository.findCaregiver(username);
        return caregiver;
    }

    public String getPatientWithUsername(String username){
        String patient = userRepository.findPatient(username);
        return patient;
    }


    public CaregiverDTO findCaregiverById(UUID id) {
        Optional<Caregiver> prosumerOptional = caregiverRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Caregiver with id {} was not found in db", id);
            throw new ResourceNotFoundException(Patient.class.getSimpleName() + " with ID: " + id);
        }
        return CaregiverBuilder.toCaregiverDTO(prosumerOptional.get());
    }

    public List<PatientCaregiverDTO> findPatientCaregiver(){
        List<Patient> patientList = patientRepository.findAll();
        List<PatientCaregiverDTO> pcList =new ArrayList<>();
        for(Patient p: patientList){
            PatientDTO patientDTO = PatientBuilder.toPatientDTO(p);
            String patientName = patientDTO.getName();
            UUID caregiverID = patientDTO.getCaregiver().getId();
            CaregiverDTO c= findCaregiverById(caregiverID);
            PatientCaregiverDTO pc = new PatientCaregiverDTO(patientName,c.getName());
            pcList.add(pc);
        }
        return pcList;
    }


    public PatientDTO findPatientById(UUID id) {
        Optional<Patient> prosumerOptional = patientRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Patient with id {} was not found in db", id);
            throw new ResourceNotFoundException(Patient.class.getSimpleName() + " with ID: " + id);
        }
        return PatientBuilder.toPatientDTO(prosumerOptional.get());
    }

    public List<PatientDTO> findPatientForC(String name) {
        Optional<Caregiver> caregiver = caregiverRepository.findByName(name);
        List<Patient> patientList = patientRepository.findAll();

        List<PatientDTO> pList =new ArrayList<>();

        for(Patient p: patientList){
            PatientDTO patientDTO = PatientBuilder.toPatientDTO(p);
            String patientName = patientDTO.getName();
            UUID caregiverID = patientDTO.getCaregiver().getId();
            CaregiverDTO c= findCaregiverById(caregiverID);
            if((c.getName()).equals(name))
                pList.add(patientDTO);
        }
        return pList;
    }

    public String getRole(String username){
        User user = userRepository.findByUsername(username);
        System.out.println(user.getUsername());
        System.out.println(user.getUserRole());
        return user.getUserRole();
    }




}
