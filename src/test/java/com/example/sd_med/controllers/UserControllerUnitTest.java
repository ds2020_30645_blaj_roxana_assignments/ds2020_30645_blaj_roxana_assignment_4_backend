package com.example.sd_med.controllers;

import com.example.sd_med.SdMedApplicationTests;
import com.example.sd_med.dtos.*;
import com.example.sd_med.entities.Patient;
import com.example.sd_med.services.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;


import java.util.Date;
import java.util.List;

import static java.util.Collections.emptyList;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/*
public class UserControllerUnitTest extends SdMedApplicationTests {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService service;

    @Test
    public void createUserDoctorTest() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        UserDTO userDTO = new UserDTO("John_ta", "123@abv", "DOCTOR");
        DoctorDTO doctorDTO = new DoctorDTO("John Tat", "neurolog");
        UserDoctorDTO userDoctorDTO = new UserDoctorDTO(userDTO, doctorDTO);

        mockMvc.perform(post("/userr/doctor")
                .content(objectMapper.writeValueAsString(userDoctorDTO))
                .contentType("application/json"))
                .andExpect(status().isCreated());
    }*/

   /* @Test
    public void createUserPacient() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        UserDTO userDTO = new UserDTO("John_ta", "123@abv", "DOCTOR");
        Date myDate = new Date(2014, 02, 11);
        PatientDTO patientDTO = new PatientDTO("John Tat", myDate, "MALE", "cluj strada Muncii","sanatos");
        UserPatientDTO userPatientDTO = new UserPatientDTO(userDTO, patientDTO);

        mockMvc.perform(post("/userr/patient")
                .content(objectMapper.writeValueAsString(userPatientDTO))
                .contentType("application/json"))
                .andExpect(status().isCreated());
    }
*/
 /*   @Test
    public void createUserCaregiver() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        UserDTO userDTO = new UserDTO("John_ta", "123@abv", "DOCTOR");
        Date myDate = new Date(2014, 02, 11);
        CaregiverDTO caregiverDTO = new CaregiverDTO("John Tat", myDate, "MALE", "cluj strada Muncii");
        UserCaregiverDTO userCaregiverDTO = new UserCaregiverDTO(userDTO, caregiverDTO);

        mockMvc.perform(post("/userr/caregiver")
                .content(objectMapper.writeValueAsString(userCaregiverDTO))
                .contentType("application/json"))
                .andExpect(status().isCreated());
    }*/



  /*  @Test
    public void insertUDTestFailsDueToPasswordLength() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
         UserDTO userDTO = new UserDTO("John_ta", "12bv", "DOCTOR");
        DoctorDTO doctorDTO = new DoctorDTO("John Tat", "neurolog");
        UserDoctorDTO userDoctorDTO = new UserDoctorDTO(userDTO, doctorDTO);

        mockMvc.perform(post("/userr/doctor")
                .content(objectMapper.writeValueAsString(userDoctorDTO))
                .contentType("application/json"))
                .andExpect(status().is5xxServerError());
    }

    @Test
    public void insertUDTestFailsDueToUsernameLength() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        UserDTO userDTO = new UserDTO("Johta", "123@abv", "DOCTOR");
        DoctorDTO doctorDTO = new DoctorDTO("John Tat", "neurolog");
        UserDoctorDTO userDoctorDTO = new UserDoctorDTO(userDTO, doctorDTO);

        mockMvc.perform(post("/userr/doctor")
                .content(objectMapper.writeValueAsString(userDoctorDTO))
                .contentType("application/json"))
                .andExpect(status().is5xxServerError());
    }

    @Test
    public void insertUDTestFailsDueToUsernameNull() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        UserDTO userDTO = new UserDTO(null, "123@abv", "DOCTOR");
        DoctorDTO doctorDTO = new DoctorDTO("John Tat", "neurolog");
        UserDoctorDTO userDoctorDTO = new UserDoctorDTO(userDTO, doctorDTO);

        mockMvc.perform(post("/userr/doctor")
                .content(objectMapper.writeValueAsString(userDoctorDTO))
                .contentType("application/json"))
                .andExpect(status().is5xxServerError());
    }

    @Test
    public void insertUDTestFailsDueToPasswordNull() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        UserDTO userDTO = new UserDTO("johnatan", null, "DOCTOR");
        DoctorDTO doctorDTO = new DoctorDTO("John Tat", "neurolog");
        UserDoctorDTO userDoctorDTO = new UserDoctorDTO(userDTO, doctorDTO);

        mockMvc.perform(post("/userr/doctor")
                .content(objectMapper.writeValueAsString(userDoctorDTO))
                .contentType("application/json"))
                .andExpect(status().is5xxServerError());
    }

    @Test
    public void insertUDTestFailsDueToRoledNull() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        UserDTO userDTO = new UserDTO("johnatan", "bajvnevv", null);
        DoctorDTO doctorDTO = new DoctorDTO("John Tat", "neurolog");
        UserDoctorDTO userDoctorDTO = new UserDoctorDTO(userDTO, doctorDTO);

        mockMvc.perform(post("/userr/doctor")
                .content(objectMapper.writeValueAsString(userDoctorDTO))
                .contentType("application/json"))
                .andExpect(status().is5xxServerError());
    }

    @Test
    public void insertUDTestFailsDueToSpecializationNull() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        UserDTO userDTO = new UserDTO("jonathan", "bajvnevv", "DOCTOR");
        DoctorDTO doctorDTO = new DoctorDTO("John Tat", null);
        UserDoctorDTO userDoctorDTO = new UserDoctorDTO(userDTO, doctorDTO);

        mockMvc.perform(post("/userr/doctor")
                .content(objectMapper.writeValueAsString(userDoctorDTO))
                .contentType("application/json"))
                .andExpect(status().is5xxServerError());
    }

    @Test
    public void insertUDTestFailsDueToNameNull() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        UserDTO userDTO = new UserDTO("jonathan", "bajvnevv", "DOCTOR");
        DoctorDTO doctorDTO = new DoctorDTO(null, "pediatru");
        UserDoctorDTO userDoctorDTO = new UserDoctorDTO(userDTO, doctorDTO);

        mockMvc.perform(post("/userr/doctor")
                .content(objectMapper.writeValueAsString(userDoctorDTO))
                .contentType("application/json"))
                .andExpect(status().is5xxServerError());
    }*/

//}
