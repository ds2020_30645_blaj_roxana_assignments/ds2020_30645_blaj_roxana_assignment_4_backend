  package com.example.sd_med.services;

import com.example.sd_med.SdMedApplicationTests;
import com.example.sd_med.dtos.*;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import static org.springframework.test.util.AssertionErrors.assertEquals;
/*
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/test-sql/create.sql")
@Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:/test-sql/delete.sql")
public class UserServiceIntegrationTests extends SdMedApplicationTests {

   @Autowired
   UserService userService;

   @Autowired
   PatientService patientService;

   @Test
   public void testGetCorrect() {
       List<UserDTO> userDTOList = userService.findUser();
       assertEquals("Test Insert User", 2, userDTOList.size());
   }*/
    /*
    @Test
   public void testInsertCorrectWithGetById() {
       UserDTO userDTO = new UserDTO("John_ta", "123@abv", "DOCTOR");
       UUID insertedID = userService.createUser(userDTO);

       UserDTO insertedUser = new UserDTO(insertedID, userDTO.getUsername(),userDTO.getPassword(), userDTO.getRole());
       UserDTO fetchedU = userService.findUserById(insertedID);

       assertEquals("Test Inserted Person", insertedUser, fetchedU);
   }

   @Test
   public void testInsertCorrectWithGetAll() {
       UserDTO p = new UserDTO("John_ta", "123@abv", "DOCTOR");
       userService.createUser(p);

       List<UserDTO> userDTOList = userService.findUser();
       assertEquals("Test Inserted Users", 3, userDTOList.size());
   }

   @Test
   public void testDeleteUserDoctorCorrectByName(){
       //UserDTO userDTO = new UserDTO("John_ta", "123@abv", "DOCTOR");
       //UUID insertedID = userService.deleteUserByUsername(userDTO.getUsername());
       UserDTO userDTO = new UserDTO("johnatan", "bajvnevv", "DOCTOR");
       DoctorDTO doctorDTO = new DoctorDTO("John Tat", "neurolog");
       UserDoctorDTO userDoctorDTO = new UserDoctorDTO(userDTO, doctorDTO);
       userService.createUserDoctor(userDoctorDTO);
       List<UserDTO> udInsertList = userService.findUser();
       userService.deleteUserByUsername(userDoctorDTO.getUserDTO().getUsername());
       List<UserDTO> udDeleteList = userService.findUser();
       assertEquals("Test Inserted Users", 3, udInsertList.size());
       assertEquals("Test Deleted Users", 2, udDeleteList.size());
   }

   @Test
   public void testDeleteUserPatientCorrectByName(){

       UserDTO userDTO = new UserDTO("johnatan", "bajvnevv", null);
       Date myDate = new Date(2014, 02, 11);
       PatientDTO patientDTO = new PatientDTO("John Tat", myDate, "MALE", "cluj strada Muncii","sanatos");
       UserPatientDTO userPatientDTO = new UserPatientDTO(userDTO, patientDTO);

       userService.createUserPatient(userPatientDTO);
       List<UserDTO> udInsertList = userService.findUser();
       userService.deleteUserByUsername(userPatientDTO.getUserDTO().getUsername());
       List<UserDTO> udDeleteList = userService.findUser();
       List<PatientDTO> pDeleteList = patientService.findPatient();
       assertEquals("Test Inserted Users", 3, udInsertList.size());
       assertEquals("Test Deleted Users", 2, udDeleteList.size());
       assertEquals("Test Deleted Users", 1, pDeleteList.size());
   }
*/


//}